﻿// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// ----------------------------------------------------------------------------

using UnityEngine;

namespace RoboRyanTron.Unite2017.Events
{

    [CreateAssetMenu(menuName = "Events/Authorization Event")]
    public class AuthorizationEvent: AppEvent
    {
        public string Email;
        public string SessionKey;
    }
}