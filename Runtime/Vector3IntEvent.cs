﻿using UnityEngine;

namespace RoboRyanTron.Unite2017.Events
{
    [CreateAssetMenu(menuName = "Events/Vector3Int Event")]
    public class Vector3IntEvent : AppEvent
    {
        public Vector3Int Point;
    }
}