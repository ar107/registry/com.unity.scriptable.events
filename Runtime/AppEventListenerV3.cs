﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace RoboRyanTron.Unite2017.Events
{
    public class AppEventListenerV3 : AppEventListenerBase
    {
        public List<AppEventV3ListenerItem> Items;

        private void OnEnable()
        {
            foreach (var item in Items)
            {
                item.Event.RegisterListener(this);
                item.MakeCache();
            }
        }

        private void OnDisable()
        {
            foreach (var item in Items)
                item.Event.UnregisterListener(this);
        }

        public override void OnEventRaised(AppEvent gameEvent)
        {
            OnEventRaisedNonPrecise(gameEvent);
        }

        private void OnEventRaisedNonPrecise(AppEvent gameEvent)
        {
            for (int i = 0; i < Items.Count; i++)
            {
                if (!Items[i].Event.Equals(gameEvent))
                    continue;

                Items[i].Action.InvokeMethod(gameEvent);

                if (gameEvent.Processed)
                    break;
            }
        }

        private void OnEventRaisedPrecise<T>(T gameEvent) where T: AppEvent
        {
            for (int i = 0; i < Items.Count; i++)
            {
                if (!Items[i].Event.Equals(gameEvent))
                    continue;

                Items[i].Action.InvokeDelegate(gameEvent);

                if (gameEvent.Processed)
                    break;
            }
        }
    }

    [Serializable]
    public class AppEventV3ListenerItem
    {
        public AppEvent Event;
        public AppEventV3ListenerItemMethod Action;

        public AppEventV3ListenerItem()
        {
            Action = new AppEventV3ListenerItemMethod();
        }

        public void MakeCache()
        {
            if (Action == null)
                return;

            if (Event == null)
                return;

            if (Action.HasCache)
                return;

            Action.CacheMethod();
        }
    }

    [Serializable]
    public class AppEventV3ListenerItemMethod
    {
        [SerializeField]
        public GameObject TargetObject;
        [SerializeField]
        public Component TargetComponent;
        [SerializeField]
        public string TargetMethod;

        private MethodInfo _methodInfo;
        private Delegate _delegate;
        
        public bool HasCache => _methodInfo != null;

        public void CacheMethod()
        {
            _methodInfo = TargetComponent.GetType().GetMethod(TargetMethod, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.FlattenHierarchy);
        }

        public void CacheDelegate<T>() where T : AppEvent
        {
            _delegate = (Action<T>)Delegate.CreateDelegate(typeof(Action<T>), TargetComponent, _methodInfo);
        }

        public void InvokeDelegate<T>(T appEvent) where T: AppEvent
        {
            (_delegate as Action<T>)(appEvent);
        }

        public void InvokeMethod(AppEvent appEvent)
        {
            _methodInfo.Invoke(TargetComponent, new [] { appEvent });
        }
    }
}