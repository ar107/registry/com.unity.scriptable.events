﻿// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// ----------------------------------------------------------------------------

using UnityEngine;

namespace RoboRyanTron.Unite2017.Events
{
    public abstract class AppEventListenerBase : MonoBehaviour
    {
        public int Order = 100;
        public abstract void OnEventRaised(AppEvent gameEvent);
    }
}