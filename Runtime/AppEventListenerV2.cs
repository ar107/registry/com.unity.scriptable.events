﻿using System;
using System.Collections.Generic;
using UnityEngine.Events;

namespace RoboRyanTron.Unite2017.Events
{
    public class AppEventListenerV2 : AppEventListenerBase
    {
        public List<AppEventListenerItem> Items;

        private void OnEnable()
        {
            foreach (var item in Items)
            {
                if (item == null || item.Event == null)
                    continue;

                item.Event.RegisterListener(this);
            }
        }

        private void OnDisable()
        {
            foreach (var item in Items)
            {
                if (item == null || item.Event == null)
                    continue;

                item.Event.UnregisterListener(this);
            }
        }

        public override void OnEventRaised(AppEvent gameEvent)
        {
            for (int i = 0; i < Items.Count; i++)
                if (Items[i].Event.Equals(gameEvent))
                    Items[i].Action.Invoke();
        }
    }

    [Serializable]
    public class AppEventListenerItem
    {
        public AppEvent Event;
        public UnityEvent Action;
    }
}