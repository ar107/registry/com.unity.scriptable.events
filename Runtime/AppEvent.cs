﻿// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// ----------------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RoboRyanTron.Unite2017.Events
{
    [CreateAssetMenu(menuName = "Events/App Event")]
    public class AppEvent : ScriptableObject
    {
        [HideInInspector] public bool Processed = false;

        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        private List<AppEventListenerBase> _eventListeners = new List<AppEventListenerBase>();

        public void Raise()
        {
            AppEventListenerBase listener = null;

            Processed = false;

            try
            {
                for (int i = 0; i < _eventListeners.Count; i++)
                {
                    if (Processed)
                        break;

                    listener = _eventListeners[i];
                    listener.OnEventRaised(this);
                }
            }
            catch (System.Exception ex)
            {
                Debug.LogError(ex.ToString(), listener);
            }
        }

        public void RegisterListener(AppEventListenerBase listener)
        {
            if (!_eventListeners.Contains(listener))
                _eventListeners.Add(listener);

            _eventListeners = _eventListeners.OrderBy(x => x.Order).ToList();
        }

        public void UnregisterListener(AppEventListenerBase listener)
        {
            if (_eventListeners.Contains(listener))
                _eventListeners.Remove(listener);

            _eventListeners = _eventListeners.OrderBy(x => x.Order).ToList();
        }
    }
}