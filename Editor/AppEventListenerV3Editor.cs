﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace RoboRyanTron.Unite2017.Events
{
    [CustomEditor(typeof(AppEventListenerV3))]
    public class AppEventListenerV3Editor : Editor
    {
        public override void OnInspectorGUI()
        {
            var content = (AppEventListenerV3)target;

            GUILayout.Space(2);

            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("+", GUILayout.MaxWidth(30)))
            {
                if (content.Items == null)
                    content.Items = new List<AppEventV3ListenerItem>();

                Undo.RecordObject(target, "Item added to Events");
                content.Items.Add(new AppEventV3ListenerItem());
                EditorUtility.SetDirty(target);
            }

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();

            var labelStyle = new GUIStyle(EditorStyles.label)
            {
                alignment = TextAnchor.MiddleRight,
                margin = new RectOffset(0, 0, 3, 0)
            };

            GUILayout.Label("Order", labelStyle, GUILayout.MaxWidth(45));

            var textFieldStyle = new GUIStyle(EditorStyles.textField)
            {
                alignment = TextAnchor.MiddleCenter
            };

            content.Order = Convert.ToInt32(GUILayout.TextField(content.Order.ToString(), textFieldStyle, GUILayout.MaxWidth(50)));
            
            EditorGUILayout.EndHorizontal();


            EditorGUILayout.EndHorizontal();

            GUILayout.Space(5);

            var items = serializedObject.FindProperty("Items");

            for (int i = 0; i < items.arraySize; i++)
            {
                var data = items.GetArrayElementAtIndex(i);

                var @event = data.FindPropertyRelative("Event");
                var action = data.FindPropertyRelative("Action");

                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.PropertyField(@event, GUIContent.none, false, GUILayout.MaxWidth(120));

                EditorGUILayout.BeginVertical(GUILayout.MaxWidth(15));
                EditorGUILayout.EndVertical();

                DrawEditorForCall(action, @event.objectReferenceValue?.GetType());

                EditorGUILayout.BeginVertical(GUILayout.MaxWidth(20));

                if (GUILayout.Button("-", GUILayout.Width(20)))
                {
                    Undo.RecordObject(target, "Item removed from Events");
                    content.Items.RemoveAt(i);
                    EditorUtility.SetDirty(target);
                }

                if (i != 0)
                {
                    if (GUILayout.Button("↥", GUILayout.Width(20)))
                    {
                        Undo.RecordObject(target, "Item go up");

                        var item = content.Items[i];
                        content.Items.RemoveAt(i);
                        content.Items.Insert(i - 1, item);

                        EditorUtility.SetDirty(target);
                    }
                }

                if (i != items.arraySize - 1)
                {
                    if (GUILayout.Button("↧", GUILayout.Width(20)))
                    {
                        Undo.RecordObject(target, "Item go down");

                        var item = content.Items[i];
                        content.Items.RemoveAt(i);
                        content.Items.Insert(i + 1, item);

                        EditorUtility.SetDirty(target);
                    }
                }

                EditorGUILayout.EndVertical();

                EditorGUILayout.EndHorizontal();
                GUILayout.Space(5);
            }

            serializedObject.ApplyModifiedProperties();
        }

        private void DrawEditorForCall(SerializedProperty property, System.Type eventType)
        {
            var rect = EditorGUILayout.BeginVertical();
            EditorGUILayout.BeginHorizontal();

            var targetObjectProperty = property.FindPropertyRelative("TargetObject");
            var targetComponentProperty = property.FindPropertyRelative("TargetComponent");
            var targetMethodProperty = property.FindPropertyRelative("TargetMethod");

            EditorGUILayout.PropertyField(targetObjectProperty, GUIContent.none, false, GUILayout.MaxWidth(120));

            if (targetObjectProperty.objectReferenceValue == null)
            {
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.EndVertical();
                return;
            }

            var targetObject = (GameObject)targetObjectProperty.objectReferenceValue;

            var beh = targetObject.GetComponents<Component>();

            int index = 0;
            Component targetComponent;

            if (targetComponentProperty.objectReferenceValue != null)
            {
                targetComponent = (Component)targetComponentProperty.objectReferenceValue;

                index = ArrayUtility.IndexOf(beh, targetComponent);
                if (index <= 0)
                    index = 0;
            }

            targetComponentProperty.objectReferenceValue = beh[EditorGUILayout.Popup(index, beh.Select(x => x.GetType().Name).ToArray())];

            EditorGUILayout.EndHorizontal();

            if (targetComponentProperty.objectReferenceValue == null)
                return;

            EditorGUILayout.BeginHorizontal();

            if (eventType == null)
                EditorGUILayout.LabelField("Choose event first", GUILayout.MaxWidth(120));
            else
                DrawEditorForMethod(targetComponentProperty, targetMethodProperty, eventType);

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();
        }

        public void DrawEditorForMethod(SerializedProperty targetComponentProperty, SerializedProperty targetMethodProperty, System.Type eventType)
        {
            var targetComponent = (Component)targetComponentProperty.objectReferenceValue;

            var targetType = targetComponent.GetType();
            var baseTargetType = targetType.BaseType;

            var methodsData = targetType
                    .GetMethods(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.FlattenHierarchy)
                    .Where(x => x.DeclaringType == targetType || x.DeclaringType == baseTargetType)
                    .Select(x => new
                    {
                        Method = x.Name,
                        Params = x.GetParameters()
                    })
                    .ToArray();

            int index = 0;
            var methods = methodsData.Where(x => x.Params.Any(y => y.ParameterType == eventType)).Select(x => x.Method).ToArray();

            if (methods.Length == 0)
            {
                EditorGUILayout.LabelField("No fit methods found", GUILayout.MaxWidth(120));
                return;
            }

            try
            {
                index = methods
                    .Select((v, i) => new { Name = v, Index = i })
                    .First(x => x.Name == targetMethodProperty.stringValue)
                    .Index;
            }
            catch { }

            targetMethodProperty.stringValue = methods[EditorGUILayout.Popup(index, methods, GUILayout.MaxWidth(120))];
        }
    }
}