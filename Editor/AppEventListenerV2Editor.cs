﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace RoboRyanTron.Unite2017.Events
{
    [CustomEditor(typeof(AppEventListenerV2))]
    public class AppEventListenerV2Editor : Editor
    {
        public override void OnInspectorGUI()
        {
            var content = (AppEventListenerV2)target;

            GUILayout.Space(2);

            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("+", GUILayout.MaxWidth(30)))
            {
                if (content.Items == null)
                    content.Items = new List<AppEventListenerItem>();

                Undo.RecordObject(target, "Item added to Events");
                content.Items.Add(new AppEventListenerItem());
                EditorUtility.SetDirty(target);
            }

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();

            var labelStyle = new GUIStyle(EditorStyles.label)
            {
                alignment = TextAnchor.MiddleRight,
                margin = new RectOffset(0, 0, 3, 0)
            };

            GUILayout.Label("Order", labelStyle, GUILayout.MaxWidth(45));

            var textFieldStyle = new GUIStyle(EditorStyles.textField)
            {
                alignment = TextAnchor.MiddleCenter
            };

            content.Order = Convert.ToInt32(GUILayout.TextField(content.Order.ToString(), textFieldStyle, GUILayout.MaxWidth(50)));

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.EndHorizontal();

            GUILayout.Space(5);

            var items = serializedObject.FindProperty("Items");
            
            for (int i = 0; i < items.arraySize; i++)
            {
                var data = items.GetArrayElementAtIndex(i);

                var @event = data.FindPropertyRelative("Event");
                var action = data.FindPropertyRelative("Action");

                EditorGUILayout.BeginHorizontal();


                EditorGUILayout.PropertyField(@event, GUIContent.none, false, GUILayout.MaxWidth(120));
                EditorGUILayout.PropertyField(action, GUIContent.none, false, GUILayout.MinWidth(100));

                EditorGUILayout.BeginVertical(GUILayout.MaxWidth(20));

                if (GUILayout.Button("-", GUILayout.Width(20)))
                {
                    Undo.RecordObject(target, "Item removed from Events");
                    content.Items.RemoveAt(i);
                    EditorUtility.SetDirty(target);
                }

                if (i != 0)
                {
                    if (GUILayout.Button("↥", GUILayout.Width(20)))
                    {
                        Undo.RecordObject(target, "Item go up");

                        var item = content.Items[i];
                        content.Items.RemoveAt(i);
                        content.Items.Insert(i - 1, item);

                        EditorUtility.SetDirty(target);
                    }
                }

                if (i != items.arraySize - 1)
                {
                    if (GUILayout.Button("↧", GUILayout.Width(20)))
                    {
                        Undo.RecordObject(target, "Item go down");

                        var item = content.Items[i];
                        content.Items.RemoveAt(i);
                        content.Items.Insert(i + 1, item);

                        EditorUtility.SetDirty(target);
                    }
                }

                EditorGUILayout.EndVertical();

                EditorGUILayout.EndHorizontal();
                GUILayout.Space(5);
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}